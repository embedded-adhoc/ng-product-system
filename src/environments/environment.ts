// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { APP_ID } from '@angular/core';

export const environment = {
  production: false,
  //api_http_server :'http://127.0.0.1:8181'
  api_http_server: 'http://192.168.0.149:8181',
  api_auth_server: 'http://192.168.0.149:8181/api/auth/', 
  api_api_test_server: 'http://192.168.0.149:8181/api/test/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
