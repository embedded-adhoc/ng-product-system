import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './_tutorial/register/register.component';
import { LoginComponent } from './_tutorial/login/login.component';
import { HomeComponent } from './_tutorial/home/home.component';
import { ProfileComponent } from './_tutorial/profile/profile.component';
import { BoardModeratorComponent } from './_tutorial/board-moderator/board-moderator.component';
import { BoardAdminComponent } from './_tutorial/board-admin/board-admin.component';
import { PageNotFoundComponent } from './_tutorial/pagenotfound/pagenotfound.component'
import { HistoryComponent } from './_tutorial/history/history.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'mod', component: BoardModeratorComponent },
  //show admin board 
  { path: 'admin', component: BoardAdminComponent },
  { path: 'timestamps', component: HistoryComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }