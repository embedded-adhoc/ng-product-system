import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';
import { Customer } from './_helpers/customer';
import { CustomersHttpService } from './_services/customershttpservice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  selectedCustomer: Customer = {};
  selectedCustomerId: number;

  constructor(private tokenStorageService: TokenStorageService, private myCustomersHttpService: CustomersHttpService) { }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();

      this.selectedCustomerId = this.tokenStorageService.getUserId();

      this.receiveCustomerInformation();

      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }
  }

  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }


  //receive info from selected customer using the selected customer id
  async receiveCustomerInformation() {
    if (this.selectedCustomerId != undefined) {
      let customer = await this.myCustomersHttpService.getOneCustomer(this.selectedCustomerId);
      this.selectedCustomer = customer;
      console.log(customer);
    }
  }
}