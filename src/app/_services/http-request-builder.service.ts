import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';

export enum HttpType {
  get = 'GET',
  put = 'PUT',
  delete = 'DELETE',
  post = 'POST',
}


@Injectable({
  providedIn: 'root'
})
export class HttpRequestBuilderService {
  constructor(private httpClient: HttpClient) {


  }

  public build(requestType: HttpType, url: string, withResBody: boolean, arg?: any): Promise<any> {
    const header = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    let request: Observable<any>;
    switch (requestType) {
      case HttpType.get:
        request = this.httpClient.get(url, {headers: header, observe: 'response'});
        break;
      case HttpType.post:
        request = this.httpClient.post(url, arg, {headers: header, observe: 'response'});
        break;
      case HttpType.put:
        request = this.httpClient.put(url, arg, {headers: header, observe: 'response'});
        break;
      case HttpType.delete:
        request = this.httpClient.delete(url, {headers: header, observe: 'response'});
        break;
    }
    return request.pipe(
      map(res => withResBody ? res.body : res.status),
      catchError(this.handleError)
    ).toPromise();
  }

  private handleError(error: any) {
    console.error(error);
    return throwError(error);
  }
}
