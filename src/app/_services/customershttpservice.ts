import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HttpRequestBuilderService, HttpType } from './http-request-builder.service';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Customer } from '../_helpers/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersHttpService {

  private API_HTTP_LOCALHOST = environment.api_http_server + '/';
  private API_URL_CUSTOMERS = "customers"
  private API_URL_PRODUCTS = "products"
  private API_URL_ROLES = "roles"

  constructor(private httpClient: HttpClient, private httpRequestBuilder: HttpRequestBuilderService) { }

  //getAllCustomers()
  public send_get_customers_request(): Observable<any> {
    console.log(' GET ALL CUSTOMERS Request sent');
    return this.httpClient.get(`${this.API_HTTP_LOCALHOST}` + `${this.API_URL_CUSTOMERS}`);
  }

  public getAllCustomers(): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS);
    return this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_CUSTOMERS, true);
  }

  public getAllRoles(): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "/" + this.API_URL_ROLES);
    return this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "/" + this.API_URL_ROLES, true);
  }

  public updateRoles(id: number, updatedRights: string) {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "/" + this.API_URL_ROLES + "=" + id);
    return this.httpRequestBuilder.build(HttpType.put,
      environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "/" + this.API_URL_ROLES + "=" + id, true,
      updatedRights);
  }

  public postNewCustomer(customer: Customer): Promise<any> {
    console.log(customer + '\n');
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + '\n');
    return this.httpRequestBuilder.build(HttpType.post,
      environment.api_http_server + "/" + this.API_URL_CUSTOMERS,
      true,
      customer);
  }

  public getOneCustomer(id: number): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + id);
    let promise = this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + id, true);
    console.log(Promise.resolve(promise));
    return promise;
  }

  public replaceCustomer(id: number, updatedCustomer: Customer): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + id);
    console.log(updatedCustomer);
    return this.httpRequestBuilder.build(HttpType.put, environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + id, true, updatedCustomer);
  }

  public deleteCustomer(id: number): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + id);
    return this.httpRequestBuilder.build(HttpType.delete, environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + id, true);
  }

  public customerBoughtProduct(selectedCustomerId: number, selectedProductId: number): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + selectedCustomerId + "/" + this.API_URL_PRODUCTS + "=" + selectedProductId);
    return this.httpRequestBuilder.build(HttpType.put, environment.api_http_server + "/" + this.API_URL_CUSTOMERS + "=" + selectedCustomerId + "/" + this.API_URL_PRODUCTS + "=" +
      selectedProductId, true);
  }

  public deleteAllCustomers(): Promise<any> {
    console.log("Deleting ALL Customers from DB");
    return this.httpRequestBuilder.build(HttpType.delete, environment.api_http_server + "/" + this.API_URL_CUSTOMERS, true);
  }

}
