import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const AUTH_API = environment.api_auth_server;


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    this.parseRoleInput(user);
    console.log("user role in signup ng: " + user.role)
    return this.http.post(AUTH_API + 'signup', {
      //left side has to be named as Java variables in repository classes
      username: user.username,
      userLastName: user.lastName,
      email: user.email,
      role: [user.role],
      password: user.password
    }, httpOptions);
  }

  changePassword(user, enteredCurrentPassword, newPassword): Observable<any> {
    console.log(user.username)
    console.log(enteredCurrentPassword)
    console.log(newPassword)
    return this.http.post(AUTH_API + 'pass', {
      username: user.username,
      currentPassword: enteredCurrentPassword,
      newPassword: newPassword
    }, httpOptions);
  }

  resetUserPassword(user, newPassword): Observable<any> {
    console.log(user.username)
    console.log(user.password)
    console.log(newPassword)
    return this.http.post(AUTH_API + 'reset', {
      username: user.username,
      currentPassword: user.password,
      newPassword: newPassword
    }, httpOptions);
  }

  parseRoleInput(user) {
    console.log(user.role);
    switch (user.role) {
      //if no other role is given as input, continue with blank role
      case undefined: user.role = ""
        break;
    }
  }

}