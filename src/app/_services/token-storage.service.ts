import { Injectable } from '@angular/core';
import { id_ID } from 'ng-zorro-antd/i18n';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  signOut() {
    window.sessionStorage.clear();
    console.log(window.sessionStorage);
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public isLoggedIn() {
    console.log(localStorage.getItem(TOKEN_KEY) != null)
    return localStorage.getItem(TOKEN_KEY) != null;
  }

  public saveUser(user) {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser() {
    console.log(JSON.parse(sessionStorage.getItem(USER_KEY)));
    return JSON.parse(sessionStorage.getItem(USER_KEY));
  }

  public getUserId() {
    let user = this.getUser();
    console.log("getUserId: " + user.id);
    return user.id;
  }

}