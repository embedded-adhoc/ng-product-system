import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HttpRequestBuilderService, HttpType } from './http-request-builder.service';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Product } from '../_helpers/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsHttpService {

  private API_HTTP_LOCALHOST= environment.api_http_server + '/';
  private API_URL_PRODUCTS =  "products"

  constructor(private httpClient: HttpClient, private httpRequestBuilder: HttpRequestBuilderService) { }

  public send_get_products_request(): Observable<any> {
    console.log(' GET ALL PRODUCTS Request sent');
    return this.httpClient.get(`${this.API_HTTP_LOCALHOST}` + `${this.API_URL_PRODUCTS}`);
  }

  public getAllProducts(): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_PRODUCTS);
    return this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_PRODUCTS, true);
  }

  public postNewProduct(product: Product): Promise<any> {
    console.log(product + '\n');
    console.log(environment.api_http_server + "/" + this.API_URL_PRODUCTS + '\n');
    return this.httpRequestBuilder.build(HttpType.post,
      environment.api_http_server + "/" + this.API_URL_PRODUCTS,
      true,
      product);
  }

  public getOneProduct(id: number): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_PRODUCTS + "=" + id);
    return this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_PRODUCTS + "=" + id, true);
  }

  public replaceProduct(id: number, updatedProduct: Product): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_PRODUCTS + "=" + id);
    console.log(updatedProduct);
    return this.httpRequestBuilder.build(HttpType.put, environment.api_http_server + "/" + this.API_URL_PRODUCTS + "=" + id, true, updatedProduct);
  }

  public deleteProduct(id: number): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_PRODUCTS + "=" + id);
    return this.httpRequestBuilder.build(HttpType.delete, environment.api_http_server + "/" + this.API_URL_PRODUCTS + "=" + id, true);
  }

  public deleteAllProducts(): Promise<any> {
    console.log("Deleting ALL Products from DB");
    return this.httpRequestBuilder.build(HttpType.delete, environment.api_http_server + "/" + this.API_URL_PRODUCTS, true);
  }

}
