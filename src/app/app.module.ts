import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LoginComponent } from './_tutorial/login/login.component';
import { RegisterComponent } from './_tutorial/register/register.component';
import { HomeComponent } from './_tutorial/home/home.component';
import { ProfileComponent } from './_tutorial/profile/profile.component';
import { BoardAdminComponent } from './_tutorial/board-admin/board-admin.component';
import { BoardModeratorComponent } from './_tutorial/board-moderator/board-moderator.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { PageNotFoundComponent } from './_tutorial/pagenotfound/pagenotfound.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { HistoryComponent } from './_tutorial/history/history.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent, RegisterComponent, HomeComponent, ProfileComponent, BoardAdminComponent, BoardModeratorComponent, PageNotFoundComponent, HistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,

    NzCardModule, NzGridModule, NzCarouselModule, NzIconModule, NzSpaceModule, NzTableModule, NzModalModule, NzCheckboxModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
