export interface Customer {
  id?: number;
  firstName?: string;
  lastName?: string;
  balance?: number;
  username?: string;
  email?: string;
  password?: string;
  isLoaded?: boolean;
  roles?: Set<String>;
  rolesInString?: Array<String>;
}