export interface Product {
    id?: number;
    name?: string;
    price?: number;
    //amount?: number;
    isLoaded?: boolean;
    orderAmount?: number;
  }