import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Customer } from '../../_helpers/customer';
import { Product } from '../../_helpers/product';
import { CustomersHttpService } from '../../_services/customershttpservice';
import { ProductsHttpService } from '../../_services/productshttpservice';
import { TokenStorageService } from '../../_services/token-storage.service'
import { TimeStampsHttpService } from '../history/history.timestampshttpservice'
import { HistoryEntry } from '../history/historyEntry'
import { HistoryComponent } from '../history/history.component';
import { NzCarouselComponent } from 'ng-zorro-antd/carousel';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  //ViewChild creates an object in typescript from html, so we can access html via ts
  @ViewChild(NzCarouselComponent, { static: false }) myCarousel: NzCarouselComponent;

  content = ''

  selectedCustomer: Customer = {
    isLoaded: false
  }; /*Variable: type */

  selectedCustomerId: number;
  selectedProductId: number;

  result: any;

  selectedProduct: Product = {
    isLoaded: false
  }; /*Variable: type */

  products: Product[] = [];

  firstSixProducts: Product[];
  secondSixProducts: Product[];

  productsArray: Product[][] = [];

  shoppingCart: Product[] = [];

  shoppingCartString: String = "";

  shoppingCartValue: number = 0;

  withBreaks = "Hello World. \n My name is Jennifer. \n What is your name?"

  constructor(private userService: UserService, private myCustomersHttpService: CustomersHttpService, private myProductsHttpService: ProductsHttpService, private myTokenStorageService: TokenStorageService, private myTimeStampsHttpService: TimeStampsHttpService) { }

  ngOnInit() {
    this.userService.getUserBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    this.selectedCustomerId = this.myTokenStorageService.getUserId();

    console.log(this.selectedCustomer);
    console.log(this.selectedProduct);
    this.refresh();

    this.dataOperation();
  }


  onSelectProduct(Product: Product): void {
    this.selectedProduct = Product;
    this.selectedProduct.isLoaded = true;
    this.selectedProductId = this.selectedProduct.id;
    //console.log(this.selectedProduct);
  }

  //receive info from selected customer using the selected customer id
  async receiveCustomerInformation() {
    if (this.selectedCustomerId != undefined) {
      let customer = await this.myCustomersHttpService.getOneCustomer(this.selectedCustomerId);
      this.selectedCustomer = customer;
      console.log(customer);
    }
  }

  //receive info from selected product using selected prod id
  async receiveProductDetails() {
    if (this.selectedProductId != undefined) {
      let product = await this.myProductsHttpService.getOneProduct(this.selectedProductId);
      this.selectedProduct = product;
      console.log(product);
    }
  }

  async OnGetAllProductsButtonClick() {
    let result = await this.myProductsHttpService.getAllProducts();
    this.products = result;
    console.log(this.products);
  }

  refresh() {
    this.OnGetAllProductsButtonClick();
    this.receiveCustomerInformation();
    this.receiveProductDetails();
  }

  async dataOperation() {
    let result = await this.myProductsHttpService.getAllProducts();
    this.products = result;

    this.products.forEach(p => p.orderAmount = 0);
    this.products.forEach(p => p.isLoaded = true);

    var temparray: Product[], chunkSize: number = 6;
    for (let i = 0; i < this.products.length; i += chunkSize) {
      temparray = this.products.slice(i, i + chunkSize);
      this.productsArray.push(temparray);
    }

  }

  intoShoppingCart(product: Product) {
    this.shoppingCart.push(product);
    console.log(product.name + " added to cart");
    this.updateShoppingCart();
  }

  removeFromShoppingCart(product: Product) {

    if (this.shoppingCart.includes(product)) {
      let index = this.shoppingCart.indexOf(product);
      //remove just this element of the array
      this.shoppingCart.splice(index, 1);
      this.updateShoppingCart();
    }
    else {
      console.log("Cart does not contain  " + product.name + " (anymore)");
    }

  }

  updateShoppingCart() {

    this.shoppingCartValue = 0;

    this.shoppingCartString = "";
    for (let i = 0; i < this.shoppingCart.length; i++) {
      this.shoppingCartString += (i + 1) + ")" + this.shoppingCart[i].name + " ";

      //update shoppingCartValue
      this.shoppingCartValue += this.shoppingCart[i].price;
    }

  }

  async buyEverythingInShoppingCart(shoppingCart: Product[]) {
    for (let i = 0; i < shoppingCart.length; i++) {
      let balance_before: number = this.selectedCustomer.balance;
      console.log("Balance before: " + balance_before);
      await this.myCustomersHttpService.customerBoughtProduct(this.selectedCustomerId, shoppingCart[i].id);

      let customer = await this.myCustomersHttpService.getOneCustomer(this.selectedCustomerId);

      let balance_after: number = customer.balance;
      //because of setting balance_BEFORE to this.selectedCustomer.balance in the upper part of this loop, we have to update this.selectedCustomer.balance here
      this.selectedCustomer.balance = balance_after;
      console.log("Balance after: " + balance_after)
      console.log("Bought product with id " + shoppingCart[i].id + " for customer with id " + this.selectedCustomerId);
      await this.myTimeStampsHttpService.postNewHistoryEntry(this.buildEntry(shoppingCart[i], balance_before, balance_after));
    }
    this.refresh();
    window.location.reload();

  }

  async resetShoppingCart() {
    this.shoppingCart = [];
    console.log("Shopping cart was reset successfully!");
    this.updateShoppingCart();
  }

  buildEntry(product: Product, balance_before: number, balance_after: number): HistoryEntry {
    let entry: HistoryEntry = {
      "customer_id": this.selectedCustomerId,
      "product_id": product.id,
      "product_name": product.name,
      "product_price": -product.price,
      "product_orderAmount": product.orderAmount,
      "customer_balanceBefore": balance_before,
      "customer_balanceAfter": balance_after
    }
    console.log(entry);
    return entry;
  }

  pre(){
    this.myCarousel.pre();
  }

  next(){
    this.myCarousel.next();
  }
}