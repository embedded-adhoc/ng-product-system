import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HttpRequestBuilderService, HttpType } from '../../_services/http-request-builder.service';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HistoryEntry } from './historyEntry';

@Injectable({
  providedIn: 'root'
})
export class TimeStampsHttpService {

  private API_URL_TIMESTAMPS = "timestamps"

  constructor(private httpClient: HttpClient, private httpRequestBuilder: HttpRequestBuilderService) { }
  
  public getAllTimeStamps(): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_TIMESTAMPS);
    return this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_TIMESTAMPS, true);
  }

  public postNewHistoryEntry(entry: HistoryEntry): Promise<any> {
    console.log(environment.api_http_server + "/" + this.API_URL_TIMESTAMPS);
    return this.httpRequestBuilder.build(HttpType.post, environment.api_http_server + "/" + this.API_URL_TIMESTAMPS, true, entry);
  }

  public getUserHistory(id: number): Promise<any> {
    console.log("Get user history from url: " + environment.api_http_server + "/" + this.API_URL_TIMESTAMPS);
    return this.httpRequestBuilder.build(HttpType.get, environment.api_http_server + "/" + this.API_URL_TIMESTAMPS + "=" + id, true);
  }

}