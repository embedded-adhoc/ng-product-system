export interface HistoryEntry {
  date?: Date;
  customer_id?: number;
  product_id?: number;
  product_name?: string;
  product_price?: number;
  product_orderAmount?: number;
  customer_balanceBefore?: number;
  customer_balanceAfter?: number;
}

