import { Component, OnInit, Injectable } from '@angular/core';
import { TimeStampsHttpService } from './history.timestampshttpservice'
import { HistoryEntry } from './historyEntry';
import { UserService } from "../../_services/user.service"
import { Customer } from '../../_helpers/customer';
import { TokenStorageService } from 'src/app/_services/token-storage.service';


@Component({
  selector: 'app-timestamps',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  result: HistoryEntry[];

  selectedCustomerId: number;

  constructor(private myTimeStampsHttpService: TimeStampsHttpService, private userService: UserService, private myTokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.selectedCustomerId = this.myTokenStorageService.getUserId();
    console.log(this.selectedCustomerId);
    this.loadUserHistory();
  }

  async loadFullHistory() {
    this.result = await this.myTimeStampsHttpService.getAllTimeStamps();
    console.log(this.result.length);
    console.log(this.result);
  }

  async loadUserHistory() {
    this.result = await this.myTimeStampsHttpService.getUserHistory(this.selectedCustomerId);
    console.log(this.result.length);
    console.log(this.result);
  }

}
