import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenStorageService } from '../../_services/token-storage.service';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/_services/auth.service';
import { environment } from 'src/environments/environment';
import { Customer } from '../../_helpers/customer';
import { CustomersHttpService } from '../../_services/customershttpservice';


const AUTH_API = environment.api_auth_server;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentUser: any;

  enteredCurrentPassword: string = "";

  newPassword: string = "";

  isLoggedIn = false;

  isLoginFailed = false;

  errorMessage = '';

  roles: string[] = [];

  selectedCustomer: Customer = {};

  selectedCustomerId: number;

  constructor(private tokenStorageService: TokenStorageService, private http: HttpClient, private authService: AuthService, private myCustomersHttpService: CustomersHttpService) { }

  ngOnInit() {
    this.currentUser = this.tokenStorageService.getUser();
    this.selectedCustomerId = this.tokenStorageService.getUserId();

    this.receiveCustomerInformation();
  }

  onSubmit() {

    this.authService.changePassword(this.currentUser, this.enteredCurrentPassword, this.newPassword).subscribe(
      data => {
        this.tokenStorageService.saveToken(data.accessToken);
        this.tokenStorageService.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.currentUser = this.tokenStorageService.getUser();
        this.roles = this.tokenStorageService.getUser().roles;
        console.log("Password changed successfully!")
        // window.location.reload();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  //receive info from selected customer using the selected customer id
  async receiveCustomerInformation() {
    if (this.selectedCustomerId != undefined) {
      let customer = await this.myCustomersHttpService.getOneCustomer(this.selectedCustomerId);
      this.selectedCustomer = customer;
      console.log(customer);
    }
  }








}