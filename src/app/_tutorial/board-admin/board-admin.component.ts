import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { Customer } from '../../_helpers/customer';
import { Product } from '../../_helpers/product';
import { CustomersHttpService } from '../../_services/customershttpservice';
import { ProductsHttpService } from '../../_services/productshttpservice';
import { AuthService } from '../../_services/auth.service'
import { TimeStampsHttpService } from '../../_tutorial/history/history.timestampshttpservice'
import { HistoryEntry } from "../history/historyEntry";
import { TokenStorageService } from 'src/app/_services/token-storage.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {
  content = '';

  selectedCustomer: Customer = {
    isLoaded: false
  }; /*Variable: type */
  customers: Customer[] = [];

  selectedCustomerId: number;
  selectedProductId: number;

  newCustomer: Customer = {};

  result: any;

  updatedCustomer: Customer = {};

  selectedProduct: Product = {
    isLoaded: false
  }; /*Variable: type */
  products: Product[] = [];

  newProduct: Product = {};

  updatedProduct: Product = {};

  constructor(private userService: UserService,
    private myCustomersHttpService: CustomersHttpService,
    private myProductsHttpService: ProductsHttpService,
    private authService: AuthService,
    private myTimeStampsHttpService: TimeStampsHttpService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    this.userService.getAdminBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    console.log(this.selectedCustomer);
    this.refresh();
  }

  onSelectCustomer(customer: Customer): void {
    this.selectedCustomer = customer;
    this.selectedCustomer.isLoaded = true;
    this.selectedCustomerId = this.selectedCustomer.id;
  }

  onSelectProduct(Product: Product): void {
    this.selectedProduct = Product;
    this.selectedProduct.isLoaded = true;
    this.selectedProductId = this.selectedProduct.id;
  }

  //getAllCustomers()
  async OnGetAllCustomersButtonClick() {
    let result: Customer[] = await this.myCustomersHttpService.getAllCustomers();
    this.customers = result;
    result.forEach(customer => customer.rolesInString = this.removeDuplicates(customer));
    console.log(result);
  }

  //newCustomer()
  async OnPostNewCustomerButtonClick() {
    await this.myCustomersHttpService.postNewCustomer(this.newCustomer);
    this.refresh();
  }

  //replaceCustomer()
  async OnReplaceCustomerButtonClick() {
    await this.myCustomersHttpService.replaceCustomer(this.selectedCustomerId, this.updatedCustomer);
    this.refresh();
  }

  //deleteOneCustomer()
  async OnDeleteOneCustomerButtonClick(customerId: number) {
    await this.myCustomersHttpService.deleteCustomer(customerId);
    this.refresh();
  }

  refresh() {
    this.OnGetAllCustomersButtonClick();
    this.OnGetAllProductsButtonClick();
  }

  async OnGetAllProductsButtonClick() {
    let result = await this.myProductsHttpService.getAllProducts();
    this.products = result;
    console.log(result)
  }

  //newProduct()
  async OnPostNewProductButtonClick() {
    await this.myProductsHttpService.postNewProduct(this.newProduct);
    this.refresh();
  }

  //getOneProduct()
  async OnGetOneProductButtonClick() {
    await this.myProductsHttpService.getOneProduct(this.selectedProductId);
  }

  //replaceProduct()
  async OnReplaceProductButtonClick() {
    await this.myProductsHttpService.replaceProduct(this.selectedProductId, this.updatedProduct);
    this.refresh();
  }

  //deleteOneProduct()
  async OnDeleteOneProductButtonClick(productId: number) {
    await this.myProductsHttpService.deleteProduct(productId);
    this.refresh();
  }

  async OnDeleteAllProductsButtonClick() {
    await this.myProductsHttpService.deleteAllProducts();
    this.refresh();
  }


  //dialog behavior

  isVisible = false;
  isOkLoading = false;

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isOkLoading = true;
    this.onSubmit();
    setTimeout(() => {
      this.isVisible = false;
      this.isOkLoading = false;
    }, 500);
  }

  handleCancel(): void {
    this.isVisible = false;
    this.isSignUpFailed = true;
  }


  //sign up new customer as admin 
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  onSubmit() {
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.refresh();
        window.location.reload();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  //replace-dialog 
  replaceVisible = false;
  replaceIsOkLoading = false;

  //bug: cancel button useless because of two way data binding 
  showReplaceModal(customer: Customer): void {
    this.updatedCustomer = customer;
    this.replaceVisible = true;
  }

  handleReplaceOk(): void {
    this.replaceIsOkLoading = true;
    this.OnReplaceCustomerButtonClick();
    setTimeout(() => {
      this.replaceVisible = false;
      this.replaceIsOkLoading = false;
    }, 500);

  }

  handleReplaceCancel(): void {
    this.replaceVisible = false;
  }

  //BALANCE INCREASEMENT 

  //dialog logic
  balanceVisible = false;
  balanceIsOkLoading = false;

  showBalanceDialog(customer: Customer): void {
    this.updatedCustomer = customer;
    console.log(this.updatedCustomer);
    this.balanceVisible = true;
  }

  handleBalanceOk(): void {
    this.balanceIsOkLoading = true;
    this.increaseCustomerBalance(this.updatedCustomer);
    setTimeout(() => {
      this.balanceVisible = false;
      this.balanceIsOkLoading = false;
    }, 500);

  }

  handleBalanceCancel(): void {
    this.balanceVisible = false;
  }

  //balance increasement logic
  balance_before: number;
  balance_after: number;
  depositMoney: number;

  async increaseCustomerBalance(customer: Customer) {
    this.balance_before = customer.balance;
    customer.balance += this.depositMoney;
    this.balance_after = customer.balance;
    await this.myCustomersHttpService.replaceCustomer(customer.id, customer);
    console.log("new balance: " + customer.balance);
    await this.myTimeStampsHttpService.postNewHistoryEntry(this.buildEntryAfterDeposit(customer));
  }

  buildEntryAfterDeposit(customer: Customer): HistoryEntry {
    let entry: HistoryEntry = {
      "customer_id": customer.id,
      "product_id": 1337,
      "product_name": "Geld aufladen",
      "product_price": this.depositMoney,
      "product_orderAmount": 1,
      "customer_balanceBefore": this.balance_before,
      "customer_balanceAfter": customer.balance
    }
    console.log(entry);
    return entry;
  }


  //dialog logic 

  passwordResetVisible = false;
  passwordResetIsOkLoading = false;

  showPasswordResetDialog(customer: Customer): void {
    this.updatedCustomer = customer;
    console.log(this.updatedCustomer);
    this.passwordResetVisible = true;
  }

  handlePasswordResetOk(): void {
    this.passwordResetIsOkLoading = true;
    this.resetUserPassword(this.updatedCustomer);
    setTimeout(() => {
      this.passwordResetVisible = false;
      this.passwordResetIsOkLoading = false;
    }, 500);

  }

  handlePasswordResetCancel(): void {
    this.passwordResetVisible = false;
  }


  newUserPassword: string = "";
  //change user's pw

  async resetUserPassword(customer: Customer) {

    this.authService.resetUserPassword(customer, this.newUserPassword).subscribe(
      data => {
        console.log(data)
        console.log("Password changed successfully!")
        console.log("new password for " + customer.username + " = " + customer.password)
        window.location.reload();
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );

  }


  removeDuplicates(customer: Customer): Array<String> {
    const newArray = customer.rolesInString.filter((elem, i, arr) => {
      if (arr.indexOf(elem) === i) {
        return elem
      }
    })
    return newArray
  }


  //add Product dialog
  addProductVisible = false;
  addProductIsOkLoading = false;

  showAddProductModal(): void {
    this.addProductVisible = true;
  }

  handleAddProductOk(): void {
    this.addProductIsOkLoading = true;
    this.OnPostNewProductButtonClick();
    //  this.OnGetAllProductsButtonClick();
    setTimeout(() => {
      this.addProductVisible = false;
      this.addProductIsOkLoading = false;
    }, 500);

  }

  handleAddProductCancel(): void {
    this.addProductVisible = false;
  }


  //replace-dialog 
  replaceProductVisible = false;
  replaceProductIsOkLoading = false;

  //bug: cancel button useless because of two way data binding 
  showReplaceProductModal(product: Product): void {
    this.updatedProduct = product;
    this.replaceProductVisible = true;
  }

  handleReplaceProductOk(): void {
    this.replaceProductIsOkLoading = true;
    this.OnReplaceProductButtonClick();
    setTimeout(() => {
      this.replaceProductVisible = false;
      this.replaceProductIsOkLoading = false;
    }, 500);

  }

  handleReplaceProductCancel(): void {
    this.replaceProductVisible = false;
  }



  //dialog logic 

  updateRolesVisible = false;
  updateRolesIsOkLoading = false;

  updatedRights;

  showUpdateRolesDialog(customer: Customer): void {
    this.updatedCustomer = customer;
    console.log(this.updatedCustomer);
    this.updateRolesVisible = true;
  }

  handleUpdateRolesOk(): void {
    this.updateRolesIsOkLoading = true;

    console.log(this.updatedRights)

    this.myCustomersHttpService.updateRoles(this.updatedCustomer.id, this.updatedRights);

    window.location.reload();

    setTimeout(() => {
      this.updateRolesVisible = false;
      this.updateRolesIsOkLoading = false;
    }, 500);

  }

  handleUpdateRolesCancel(): void {
    this.updateRolesVisible = false;
  }



}