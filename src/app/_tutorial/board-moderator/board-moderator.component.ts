
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { CustomersHttpService } from '../../_services/customershttpservice';
import { ProductsHttpService } from '../../_services/productshttpservice';
import { Customer } from '../../_helpers/customer';
import { Product } from '../../_helpers/product';
import { TimeStampsHttpService } from '../../_tutorial/history/history.timestampshttpservice'
import { HistoryEntry } from "../history/historyEntry";

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css']
})
export class BoardModeratorComponent implements OnInit {
  content: any;

  constructor(private userService: UserService, private myCustomersHttpService: CustomersHttpService, private myProductsHttpService: ProductsHttpService, private myTimeStampsHttpService: TimeStampsHttpService) { }

  selectedCustomer: Customer = {
    isLoaded: false
  }; /*Variable: type */
  customers: Customer[] = [];

  selectedCustomerId: number;
  selectedProductId: number;

  newCustomer: Customer = {};

  result: any;

  updatedCustomer: Customer = {};

  selectedProduct: Product = {
    isLoaded: false
  }; /*Variable: type */
  products: Product[] = [];

  newProduct: Product = {};

  updatedProduct: Product = {};

  ngOnInit() {
    this.userService.getModeratorBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    console.log(this.selectedCustomer);
    console.log(this.selectedProduct);
    this.refresh();
  }


  onSelectProduct(Product: Product): void {
    this.selectedProduct = Product;
    this.selectedProduct.isLoaded = true;
    this.selectedProductId = this.selectedProduct.id;
  }

  onSelectCustomer(customer: Customer): void {
    this.selectedCustomer = customer;
    this.selectedCustomer.isLoaded = true;
    this.selectedCustomerId = this.selectedCustomer.id;
  }

  //getAllCustomers()
  async OnGetAllCustomersButtonClick() {
    let result = await this.myCustomersHttpService.getAllCustomers();
    this.customers = result;
    console.log(result)
  }

  //newCustomer()
  async OnPostNewCustomerButtonClick() {
    await this.myCustomersHttpService.postNewCustomer(this.newCustomer);
    this.refresh();
  }

  //getOneCustomer()
  async OnGetOneCustomerButtonClick() {
    await this.myCustomersHttpService.getOneCustomer(this.selectedCustomerId);
  }

  async OnGetAllProductsButtonClick() {
    let result = await this.myProductsHttpService.getAllProducts();
    this.products = result;
    console.log(result)
  }

  //newProduct()
  async OnPostNewProductButtonClick() {
    await this.myProductsHttpService.postNewProduct(this.newProduct);
    this.refresh();
  }

  //getOneProduct()
  async OnGetOneProductButtonClick() {
    await this.myProductsHttpService.getOneProduct(this.selectedProductId);
  }

  //replaceProduct()
  async OnReplaceProductButtonClick() {
    await this.myProductsHttpService.replaceProduct(this.selectedProductId, this.updatedProduct);
    this.refresh();
  }

  //deleteOneProduct()
  async OnDeleteOneProductButtonClick(productId: number) {
    await this.myProductsHttpService.deleteProduct(productId);
    this.refresh();
  }

  async OnDeleteAllProductsButtonClick() {
    await this.myProductsHttpService.deleteAllProducts();
    this.refresh();
  }

  refresh() {
    this.OnGetAllCustomersButtonClick();
    this.OnGetAllProductsButtonClick();
  }


  balanceVisible = false;
  balanceIsOkLoading = false;

  showBalanceDialog(customer: Customer): void {
    this.updatedCustomer = customer;
    console.log(this.updatedCustomer);
    this.balanceVisible = true;
  }

  handleBalanceOk(): void {
    this.balanceIsOkLoading = true;
    this.increaseCustomerBalance(this.updatedCustomer);
    setTimeout(() => {
      this.balanceVisible = false;
      this.balanceIsOkLoading = false;
    }, 500);

  }

  handleBalanceCancel(): void {
    this.balanceVisible = false;
  }

  //balance increasement logic
  balance_before: number;
  balance_after: number;
  depositMoney: number;

  async increaseCustomerBalance(customer: Customer) {
    this.balance_before = customer.balance;
    customer.balance += this.depositMoney;
    this.balance_after = customer.balance;
    await this.myCustomersHttpService.replaceCustomer(customer.id, customer);
    console.log("new balance: " + customer.balance);
    await this.myTimeStampsHttpService.postNewHistoryEntry(this.buildEntryAfterDeposit(customer));
  }

  buildEntryAfterDeposit(customer: Customer): HistoryEntry {
    let entry: HistoryEntry = {
      "customer_id": customer.id,
      "product_id": 1337,
      "product_name": "Geld aufladen",
      "product_price": this.depositMoney,
      "product_orderAmount": 1,
      "customer_balanceBefore": this.balance_before,
      "customer_balanceAfter": customer.balance
    }
    console.log(entry);
    return entry;
  }



  //replace-dialog 
  replaceVisible = false;
  replaceIsOkLoading = false;

  //bug: cancel button useless because of two way data binding 
  showReplaceModal(product: Product): void {
    this.updatedProduct = product;
    this.replaceVisible = true;
  }

  handleReplaceOk(): void {
    this.replaceIsOkLoading = true;
    this.OnReplaceProductButtonClick();
    setTimeout(() => {
      this.replaceVisible = false;
      this.replaceIsOkLoading = false;
    }, 500);

  }

  handleReplaceCancel(): void {
    this.replaceVisible = false;
  }




  //add Product dialog
  addProductVisible = false;
  addProductIsOkLoading = false;

  showAddProductModal(): void {
    this.addProductVisible = true;
  }

  handleAddProductOk(): void {
    this.addProductIsOkLoading = true;
    this.OnPostNewProductButtonClick();
  //  this.OnGetAllProductsButtonClick();
    setTimeout(() => {
      this.addProductVisible = false;
      this.addProductIsOkLoading = false;
    }, 500);

  }

  handleAddProductCancel(): void {
    this.addProductVisible = false;
  }







}